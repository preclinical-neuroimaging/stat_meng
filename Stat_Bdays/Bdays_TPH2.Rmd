---
title: "B_TPH2"
author: "Alex_Meng"
date: '2024-02-02'
output: html_document
---
## packages needed

```{r, eval=FALSE}
install.packages('tidyverse', 'glue', 'glue','lme4','multcomp','parameters', 'effectsize','performance','ggpubr')
```

## packages loading
```{r}
#General utility packages
library(tidyverse)
library(glue)
library(knitr)
library(data.table)
#Stats packages
library(lme4)
library(multcomp)
library(parameters)
library(effectsize)
library(performance)
library(emmeans)
#Plot packages
library(ggpubr)
```

## loading data
```{r}
df <- read_csv('C:/Users/xiamen/Desktop/data_for_R/Bdays/B_TPH2.csv') %>% 
          mutate(trials = as.factor(trials)) %>% 
          mutate(group = as.factor(group)) %>% 
          mutate(sex = as.factor(sex)) %>% 
          mutate(rat_ID = as.factor(rat_ID)) %>% 
          mutate(food.distance.cm = as.factor(food.distance.cm))%>%
          mutate(robot.day = as.factor(robot.day))%>%
          mutate(baseline.day = as.factor(baseline.day))%>%
         
        # Create score based on time. Following log attribution of score as a function of time. 
          mutate(leaving.score =  case_when(is.na(leaving.sec) ~ 7,
                                                  leaving.sec >= 200 ~ 6,
                                                  leaving.sec >= 100 ~ 5,
                                                  leaving.sec >= 50 ~ 4,
                                                  leaving.sec >= 25  ~ 3,
                                                  leaving.sec >= 12.5 ~ 2,
                                                  leaving.sec >= 6.25 ~ 1,
                                                  leaving.sec <6.25 ~ 0)) %>% 
            mutate(approaching.score =  case_when(is.na(approaching.sec) ~ 7,
                                                  approaching.sec >= 200 ~ 6,
                                                  approaching.sec >= 100 ~ 5,
                                                  approaching.sec >= 50 ~ 4,
                                                  approaching.sec >= 25  ~ 3,
                                                  approaching.sec >= 12.5 ~ 2,
                                                  approaching.sec >= 6.25 ~ 1,
                                                  approaching.sec <6.25 ~ 0)) %>% 
            mutate(foraging.score =  case_when(is.na(foraging.sec) ~ 7,
                                                  foraging.sec >= 200 ~ 6,
                                                  foraging.sec >= 100 ~ 5,
                                                  foraging.sec >= 50 ~ 4,
                                                  foraging.sec >= 25  ~ 3,
                                                  foraging.sec >= 12.5 ~ 2,
                                                  foraging.sec >= 6.25 ~ 1,
                                                  foraging.sec <6.25 ~ 0))

#head(df) %>% kable("pipe")
```

## leaving:
```{r}
leaving <- lmer(leaving.score~ group + trials + sex + group:trials + group:sex + trials:sex + baseline.day + baseline.day:group + baseline.day:sex + baseline.day:trials + (1|rat_ID), df)
eta_squared(leaving)
```

```{r}
Fig_leaving<- ggplot(df, aes(x=baseline.day, y=leaving.score, group=rat_ID)) + 
stat_summary(fun=mean, aes(group = sex, color= sex), geom="line", linewidth=2)+
stat_summary(fun.data = mean_se, aes(group=sex, color = sex), geom = "errorbar", alpha=1, width=0.05)+
scale_color_manual(values = c("#A6D854", "#FFD92F"))+
scale_y_continuous(breaks = seq(0, 7, by = 1))+
labs(x = "baseline days", y = "leaving score") + theme_classic()
Fig_leaving
#library(bruceR)
MANOVA(data=df, dv="leaving.score", between=c("sex", "group"))%>%
EMMEANS("sex", by="group")
#EMMEANS("group", by="sex")
```

## approaching:
```{r}
approaching <- lmer(approaching.score~ group + trials + sex + group:trials + group:sex + trials:sex + baseline.day + baseline.day:group + baseline.day:sex + baseline.day:trials + (1|rat_ID), df)
eta_squared(approaching)
```

```{r}
Fig_approaching<- ggplot(df, aes(x=baseline.day, y=approaching.score, group=rat_ID)) + 
stat_summary(fun=mean, aes(group = sex, color= sex), geom="line", linewidth=2)+
stat_summary(fun.data = mean_se, aes(group=sex, color = sex), geom = "errorbar", alpha=1, width=0.05)+
scale_color_manual(values = c("#A6D854", "#FFD92F"))+
scale_y_continuous(breaks = seq(0, 7, by = 1))+
labs(x = "baseline days", y = "approaching score") + theme_classic()
Fig_approaching
```

## foraging:
```{r}
foraging <- lmer(foraging.score~ group + trials + sex + group:trials + group:sex + trials:sex + baseline.day + baseline.day:group + baseline.day:sex + baseline.day:trials + (1|rat_ID), df)
eta_squared(foraging)
```


```{r}
Fig_foraging<- ggplot(df, aes(x=baseline.day, y=foraging.score, group=rat_ID)) + 
stat_summary(fun=mean, aes(group = group, color= group), geom="line", linewidth=2)+
stat_summary(fun.data = mean_se, aes(group=group, color = group), geom = "errorbar", alpha=1, width=0.05)+
scale_color_manual(values = c("#A6D854", "#FFD92F"))+
scale_y_continuous(breaks = seq(0, 7, by = 1))+
labs(x = "baseline days", y = "foraging score") + theme_classic()
Fig_foraging
```