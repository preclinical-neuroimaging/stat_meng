---
title: "AlexM_robogator_fluoxetine"
author: "Alex_Meng"
date: "9/19/2022"
output: html_document
---
## packages needed
```{r, eval=FALSE}
install.packages('tidyverse', 'glue', 'glue','lme4','multcomp','parameters', 'effectsize','performance','ggpubr', 'wesanderson')
```
## packages loading
```{r}
#General utility packages
library(tidyverse)
library(glue)
library(knitr)
library(glue)
#Stats packages
library(lme4)
library(multcomp)
library(parameters)
library(effectsize)
library(performance)
library(emmeans)
library(afex)
#Plot packages
library(ggpubr)
library(wesanderson)
```
## session information
```{r sessioninfo, eval=TRUE}
#output session info
sessionInfo()
```
## loading table
```{r}
df <- read_csv('C:/Users/xiamen/Desktop/data_for_R/malerday1sert.csv') %>% 
          mutate(trials = as.factor(trials)) %>% 
          mutate(group = as.factor(group)) %>% 
          mutate(sex = as.factor(sex)) %>% 
          mutate(rat_ID = as.factor(rat_ID)) %>% 
          mutate(age = as.factor(age))%>%
        # Create score based on time. Following log attribution of score as a function of time. 
mutate(leaving.score =  case_when(is.na(leaving.sec) ~ 7,
                                                  leaving.sec >= 200 ~ 6,
                                                  leaving.sec >= 100 ~ 5,
                                                  leaving.sec >= 50 ~ 4,
                                                  leaving.sec >= 25  ~ 3,
                                                  leaving.sec >= 12.5 ~ 2,
                                                  leaving.sec >= 6.25 ~ 1,
                                                  leaving.sec <6.25 ~ 0)) %>% 
mutate(approaching.score =  case_when(is.na(approaching.sec) ~ 7,
                                                  approaching.sec >= 200 ~ 6,
                                                  approaching.sec >= 100 ~ 5,
                                                  approaching.sec >= 50 ~ 4,
                                                  approaching.sec >= 25  ~ 3,
                                                  approaching.sec >= 12.5 ~ 2,
                                                  approaching.sec >= 6.25 ~ 1,
                                                  approaching.sec <6.25 ~ 0)) %>% 
mutate(backing.score =  case_when(is.na(backing.sec) ~ 7,
                                                  backing.sec >= 200 ~ 6,
                                                  backing.sec >= 100 ~ 5,
                                                  backing.sec >= 50 ~ 4,
                                                  backing.sec >= 25  ~ 3,
                                                  backing.sec >= 12.5 ~ 2,
                                                  backing.sec >= 6.25 ~ 1,
                                                  backing.sec <6.25 ~ 0))  %>%
mutate(foraging.score =  case_when(is.na(foraging.sec) ~ 7,
                                                  foraging.sec >= 200 ~ 6,
                                                  foraging.sec >= 100 ~ 5,
                                                  foraging.sec >= 50 ~ 4,
                                                  foraging.sec >= 25  ~ 3,
                                                  foraging.sec >= 12.5 ~ 2,
                                                  foraging.sec >= 6.25 ~ 1,
                                                  foraging.sec <6.25 ~ 0))
 

#head(df) %>% kable("pipe")
```
##select testing days (Rday1 here)
```{r}
test_day <- df %>% filter(df$robot.day == 1)
```
##leaving
```{r}
leaving1 <- lmer(leaving.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID), test_day)
leaving2 <- lmer(leaving.score~ group + trials + age + group:trials + age:trials + (1|rat_ID), test_day)
leaving3 <- lmer(leaving.score~ group + trials + age + group:age + age:trials + (1|rat_ID), test_day)
leaving4 <- lmer(leaving.score~ group + trials + age + group:age + group:trials + (1|rat_ID), test_day)
leaving5 <- lmer(leaving.score~ trials + age + age:trials + (1|rat_ID), test_day)
leaving6 <- lmer(leaving.score~ group + age + group:age + (1|rat_ID), test_day)
leaving7 <- lmer(leaving.score~ group + trials + group:trials + (1|rat_ID), test_day)

compare_performance(leaving1, leaving2, leaving3, leaving4, leaving5, leaving6,leaving7)
check_model(leaving1,check=c("vif", "qq", "normality","linearity","homogeneity",'reqq'))
```
##effect size
```{r}
eta_squared(leaving1)
```

##ANOVA-based significant test
```{r}
#group:age interaction effect
anova(leaving1, leaving2)
#group:trials effect
anova(leaving1, leaving3)
#age:trials effect
anova(leaving1, leaving4)
#group effect
anova(leaving1, leaving5)
#trials effect
anova(leaving1, leaving6)
#age effect
anova(leaving1, leaving7)

data1_2 <- test_day %>% filter(test_day$trials == 1|test_day$trials== 2)
data1_3 <- test_day %>% filter(test_day$trials == 1|test_day$trials== 3)
data1_4 <- test_day %>% filter(test_day$trials == 1|test_day$trials== 4)
data1_5 <- test_day %>% filter(test_day$trials == 1|test_day$trials== 5)
data2_3 <- test_day %>% filter(test_day$trials == 2|test_day$trials== 3)
data2_4 <- test_day %>% filter(test_day$trials == 2|test_day$trials== 4)
data2_5 <- test_day %>% filter(test_day$trials == 2|test_day$trials== 5)
data3_4 <- test_day %>% filter(test_day$trials == 3|test_day$trials== 4)
data3_5 <- test_day %>% filter(test_day$trials == 3|test_day$trials== 5)
data4_5 <- test_day %>% filter(test_day$trials == 4|test_day$trials== 5)

leaving1_2 <- lmer(leaving.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID),data1_2)
leaving1_3 <- lmer(leaving.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID),data1_3)
leaving1_4 <- lmer(leaving.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID),data1_4)
leaving1_5 <- lmer(leaving.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID),data1_5)
leaving2_3 <- lmer(leaving.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID), data2_3)
leaving2_4 <- lmer(leaving.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID),data2_4)
leaving2_5 <- lmer(leaving.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID),data2_5)
leaving3_4 <- lmer(leaving.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID),data3_4)
leaving3_5 <- lmer(leaving.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID),data3_5)
leaving4_5 <- lmer(leaving.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID),data4_5)

print("leaving1_2")
anova(leaving1_2)
print("leaving1_3")
anova(leaving1_3)
print("leaving1_4")
anova(leaving1_4)
print("leaving1_5")
anova(leaving1_5)
print("leaving2_3")
anova(leaving2_3)
print("leaving2_4")
anova(leaving2_4)
print("leaving2_5")
anova(leaving2_5)
print("leaving3_4")
anova(leaving3_4)
print("leaving3_5")
anova(leaving3_5)
print("leaving4_5")
anova(leaving4_5)

emmeans(leaving1, pairwise ~ trials)
emmeans(leaving1, pairwise ~ trials|group)
emmeans(leaving1, pairwise ~ group|trials)
emmeans(leaving1, pairwise ~ age|group)
emmeans(leaving1, pairwise ~ group|age)

sert_young <- test_day %>% filter(age == 70)
sert_old <- test_day %>% filter(age == 140)
leaving_young <- lmer(leaving.score~ group + trials + group:trials + (1|rat_ID), sert_young)
leaving_old <- lmer(leaving.score~ group + trials + group:trials + (1|rat_ID), sert_old)
emmeans(leaving_young, pairwise ~ trials|group)
emmeans(leaving_young, pairwise ~ group|trials)
emmeans(leaving_old, pairwise ~ trials|group)
emmeans(leaving_old, pairwise ~ group|trials)
```

##plot
```{r}
p_leaving <- ggplot(test_day, aes(x=trials, y=leaving.score, group=group, colour=group))
colors <- c("#ffd401", "#00b0eb", "#e20612", "#f1ae2d")
p_leaving +
stat_summary(fun = "mean", size = 1, geom = "line", fun.min = 'sd', fun.max = 'sd') +
stat_summary(fun.data = mean_se, geom = "errorbar", alpha=1, width=0.05, aes(colour = group, fill = group))+
scale_color_manual(values = colors)+
scale_y_continuous(breaks = seq(0, 7, by = 1))+
facet_wrap(~ age, ncol = 4) + labs(x = "positions", y = "leaving score")
#geom_jitter(alpha = 0.5,width = 0.1) + labs(x = "positions", y = "leaving score") + theme_minimal()
```

##Approaching
##factors selection
```{r}
approaching1 <- lmer(approaching.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID), test_day)
approaching2 <- lmer(approaching.score~ group + trials + age + group:trials + age:trials + (1|rat_ID), test_day)
approaching3 <- lmer(approaching.score~ group + trials + age + group:age + age:trials + (1|rat_ID), test_day)
approaching4 <- lmer(approaching.score~ group + trials + age + group:age + group:trials + (1|rat_ID), test_day)
approaching5 <- lmer(approaching.score~ trials + age + age:trials + (1|rat_ID), test_day)
approaching6 <- lmer(approaching.score~ group + age + group:age + (1|rat_ID), test_day)
approaching7 <- lmer(approaching.score~ group + trials + group:trials + (1|rat_ID), test_day)

compare_performance(approaching1, approaching2, approaching3, approaching4, approaching5, approaching6,approaching7)
check_model(approaching1,check=c("vif", "qq", "normality","linearity","homogeneity",'reqq'))
```
##effect size
```{r}
eta_squared(approaching1)
```
##ANOVA-based significant test
```{r}
#group:age interaction effect
anova(approaching1, approaching2)
#group:trials effect
anova(approaching1, approaching3)
#age:trials effect
anova(approaching1, approaching4)
#group effect
anova(approaching1, approaching5)
#trials effect
anova(approaching1, approaching6)
#age effect
anova(approaching1, approaching7)

approaching1_2 <- lmer(approaching.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID),data1_2)
approaching1_3 <- lmer(approaching.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID),data1_3)
approaching1_4 <- lmer(approaching.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID),data1_4)
approaching1_5 <- lmer(approaching.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID),data1_5)
approaching2_3 <- lmer(approaching.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID),data2_3)
approaching2_4 <- lmer(approaching.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID),data2_4)
approaching2_5 <- lmer(approaching.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID),data2_5)
approaching3_4 <- lmer(approaching.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID),data3_4)
approaching3_5 <- lmer(approaching.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID),data3_5)
approaching4_5 <- lmer(approaching.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID),data4_5)
print("approaching1_2")
anova(approaching1_2)
print("approaching1_3")
anova(approaching1_3)
print("approaching1_4")
anova(approaching1_4)
print("approaching1_5")
anova(approaching1_5)
print("approaching2_3")
anova(approaching2_3)
print("approaching2_4")
anova(approaching2_4)
print("approaching2_5")
anova(approaching2_5)
print("approaching3_4")
anova(approaching3_4)
print("approaching3_5")
anova(approaching3_5)
print("approaching4_5")
anova(approaching4_5)

emmeans(approaching1, pairwise ~ trials)
emmeans(approaching1, pairwise ~ trials|group)
emmeans(approaching1, pairwise ~ group|trials)
emmeans(approaching1, pairwise ~ age|group)
emmeans(approaching1, pairwise ~ group|age)
emmeans(approaching1, pairwise ~ age|trials)
emmeans(approaching1, pairwise ~ trials|age)

approaching_young <- lmer(approaching.score~ group + trials + group:trials + (1|rat_ID), sert_young)
approaching_old <- lmer(approaching.score~ group + trials + group:trials + (1|rat_ID), sert_old)
emmeans(approaching_young, pairwise ~ trials|group)
emmeans(approaching_young, pairwise ~ group|trials)
emmeans(approaching_old, pairwise ~ trials|group)
emmeans(approaching_old, pairwise ~ group|trials)
```
##plot
```{r}
p_approaching <- ggplot(test_day, aes(x=trials, y=approaching.score, group=group, colour=group))
colors <- c("#ffd401", "#00b0eb", "#e20612", "#f1ae2d")
p_approaching +
stat_summary(fun = "mean", size = 1, geom = "line", fun.min = 'sd', fun.max = 'sd') +
stat_summary(fun.data = mean_se, geom = "errorbar", alpha=1, width=0.05, aes(colour = group, fill = group))+
scale_color_manual(values = colors)+
scale_y_continuous(breaks = seq(0, 7, by = 1))+
facet_wrap(~ age, ncol = 4)+ labs(x = "positions", y = "approaching")
#geom_jitter(alpha = 0.5,width = 0.1) + labs(x = "positions", y = "approaching") + theme_minimal()
```

##foraging
##factors selection
```{r}
foraging1 <- lmer(foraging.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID), test_day)
foraging2 <- lmer(foraging.score~ group + trials + age + group:trials + age:trials + (1|rat_ID), test_day)
foraging3 <- lmer(foraging.score~ group + trials + age + group:age + age:trials + (1|rat_ID), test_day)
foraging4 <- lmer(foraging.score~ group + trials + age + group:age + group:trials + (1|rat_ID), test_day)
foraging5 <- lmer(foraging.score~ trials + age + age:trials + (1|rat_ID), test_day)
foraging6 <- lmer(foraging.score~ group + age + group:age + (1|rat_ID), test_day)
foraging7 <- lmer(foraging.score~ group + trials + group:trials + (1|rat_ID), test_day)

compare_performance(foraging1, foraging2, foraging3, foraging4, foraging5, foraging6, foraging7)
check_model(foraging1,check=c("vif", "qq", "normality","linearity","homogeneity",'reqq'))
```

##ANOVA-based significant test
```{r}
#group:age interaction effect
anova(foraging1, foraging2)
#group:trials effect
anova(foraging1, foraging3)
#age:trials effect
anova(foraging1, foraging4)
#group effect
anova(foraging1, foraging5)
#trials effect
anova(foraging1, foraging6)
#age effect
anova(foraging1, foraging7)

foraging1_2 <- lmer(foraging.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID),data1_2)
foraging1_3 <- lmer(foraging.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID),data1_3 )
foraging1_4 <- lmer(foraging.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID), data1_4)
foraging1_5 <- lmer(foraging.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID),data1_5 )
foraging2_3 <- lmer(foraging.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID),data2_3 )
foraging2_4 <- lmer(foraging.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID),data2_4 )
foraging2_5 <- lmer(foraging.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID),data2_5 )
foraging3_4 <- lmer(foraging.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID),data3_4 )
foraging3_5 <- lmer(foraging.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID),data3_5)
foraging4_5 <- lmer(foraging.score~ group + trials + age + group:age + group:trials + age:trials + (1|rat_ID),data4_5)
print("foraging1_2")
anova(foraging1_2)
print("foraging1_3")
anova(foraging1_3)
print("foraging1_4")
anova(foraging1_4)
print("foraging1_5")
anova(foraging1_5)
print("foraging2_3")
anova(foraging2_3)
print("foraging2_4")
anova(foraging2_4)
print("foraging2_5")
anova(foraging2_5)
print("foraging3_4")
anova(foraging3_4)
print("foraging3_5")
anova(foraging3_5)
print("foraging4_5")
anova(foraging4_5)

emmeans(foraging1, pairwise ~ trials)
emmeans(foraging1, pairwise ~ trials|group)
emmeans(foraging1, pairwise ~ group|trials)
emmeans(foraging1, pairwise ~ age|group)
emmeans(foraging1, pairwise ~ group|age)

foraging_young <- lmer(foraging.score~ group + trials + group:trials + (1|rat_ID), sert_young)
foraging_old <- lmer(foraging.score~ group + trials + group:trials + (1|rat_ID), sert_old)
emmeans(foraging_young, pairwise ~ trials|group)
emmeans(foraging_young, pairwise ~ group|trials)
emmeans(foraging_old, pairwise ~ trials|group)
emmeans(foraging_old, pairwise ~ group|trials)
```
##effect size
```{r}
eta_squared(foraging1)
```
##plot
```{r}
p_foraging <- ggplot(test_day, aes(x=trials, y=foraging.score, group=group, colour=group))
colors <- c("#ffd401", "#00b0eb", "#e20612", "#f1ae2d")
p_foraging +
stat_summary(fun = "mean", size = 1, geom = "line", fun.min = 'sd', fun.max = 'sd') +
stat_summary(fun.data = mean_se, geom = "errorbar", alpha=1, width=0.05, aes(colour = group, fill = group))+
scale_color_manual(values = colors)+
scale_y_continuous(breaks = seq(0, 7, by = 1))+
facet_wrap(~ age, ncol = 2) + labs(x = "positions", y = "foraging")
#geom_jitter(alpha = 0.5,width = 0.1) + labs(x = "positions", y = "foraging") + theme_minimal()
```