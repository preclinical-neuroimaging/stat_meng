---
title: "Rday1-Fluoxetine"
output: github_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, 
                      warning = FALSE, 
                      tidy = TRUE, 
                      eval = TRUE, 
                      include = TRUE, 
                      message = TRUE, 
                      results = "hold")
options(knitr.kable.NA = '')
```

## packages needed

```{r, eval=FALSE}
install.packages('tidyverse', 'glue', 'glue','lme4','multcomp','parameters', 'effectsize','performance','ggpubr')
```

## packages loading
```{r}
#General utility packages
library(tidyverse)
library(glue)
library(knitr)
library(data.table)
#Stats packages
library(lme4)
library(multcomp)
library(parameters)
library(effectsize)
library(performance)
library(emmeans)
#Plot packages
library(ggpubr)
```
## session information
```{r sessioninfo, eval=TRUE}
#output session info
sessionInfo()
```

## loading data
```{r}
df <- read_csv('C:/Users/xiamen/Desktop/data_for_R/Rdays/Fluoxetine.csv') %>% 
          mutate(trials = as.factor(trials)) %>% 
          mutate(group = as.factor(group)) %>% 
          mutate(sex = as.factor(sex)) %>% 
          mutate(rat_ID = as.factor(rat_ID)) %>% 
          mutate(food.distance.cm = as.factor(food.distance.cm))%>%
          mutate(robot.day = as.factor(robot.day))%>%
         
        # Create score based on time. Following log attribution of score as a function of time. 
          mutate(leaving.score =  case_when(is.na(leaving.sec) ~ 7,
                                                  leaving.sec >= 200 ~ 6,
                                                  leaving.sec >= 100 ~ 5,
                                                  leaving.sec >= 50 ~ 4,
                                                  leaving.sec >= 25  ~ 3,
                                                  leaving.sec >= 12.5 ~ 2,
                                                  leaving.sec >= 6.25 ~ 1,
                                                  leaving.sec <6.25 ~ 0)) %>% 
            mutate(approaching.score =  case_when(is.na(approaching.sec) ~ 7,
                                                  approaching.sec >= 200 ~ 6,
                                                  approaching.sec >= 100 ~ 5,
                                                  approaching.sec >= 50 ~ 4,
                                                  approaching.sec >= 25  ~ 3,
                                                  approaching.sec >= 12.5 ~ 2,
                                                  approaching.sec >= 6.25 ~ 1,
                                                  approaching.sec <6.25 ~ 0)) %>% 
            mutate(foraging.score =  case_when(is.na(foraging.sec) ~ 7,
                                                  foraging.sec >= 200 ~ 6,
                                                  foraging.sec >= 100 ~ 5,
                                                  foraging.sec >= 50 ~ 4,
                                                  foraging.sec >= 25  ~ 3,
                                                  foraging.sec >= 12.5 ~ 2,
                                                  foraging.sec >= 6.25 ~ 1,
                                                  foraging.sec <6.25 ~ 0))

#head(df) %>% kable("pipe")

```

## summary of the data
```{r}
summary(df) %>% kable("pipe")
```
## select testing days
```{r}
df_R1 <- df %>% filter(df$robot.day == 1)
```

#### model comparision
## sec/score model comparision using foraging peformance
```{r}
p_sec <- ggplot(df_R1, aes(x=trials, y=foraging.sec, group=group, colour=group, shape=sex))
p_sec + stat_summary(fun = "mean", colour = "darkgrey", size = 0.5, geom = "line", fun.min = 'sd', fun.max = 'sd')+
    stat_summary(fun.data = mean_se, geom = "ribbon", alpha=0.3, aes(colour = group, fill = group))+ geom_point(alpha = 1)+theme_minimal()

p_sco <- ggplot(df_R1, aes(x=trials, y=foraging.score, group=group, colour=group, shape=sex))
p_sco + stat_summary(fun = "mean", colour = "darkgrey", size = 0.5, geom = "line", fun.min = 'sd', fun.max = 'sd')+
    stat_summary(fun.data = mean_se, geom = "ribbon", alpha=0.3, aes(colour = group, fill = group))+ geom_point(alpha = 1)+theme_minimal()
#***here we noticed,in sec dataset, many missing values in position5, so score dataset is more accurate, besides, the score dataset seems to recapitulate sec dataset

mod.sec <- lmer(foraging.sec~ trials:sex  + trials + sex + (1|rat_ID), df_R1)
mod.score <- lmer(foraging.score~ trials:sex  + trials + sex + (1|rat_ID), df_R1)
compare_performance(mod.sec, mod.score)
#***The model with scores has a lower AIC and BIC, but comparable R2. This suggest the model with scores is preferable.

print('checking model with seconds')
check_normality(mod.sec)
check_heteroscedasticity(mod.sec)
print('checking model with scores')
check_normality(mod.score)
check_heteroscedasticity(mod.score)
#***Model with scores seem to better follow normal distribution.
```
## For Robot testing day1
## leaving:
```{r}
leaving <- lmer(leaving.score~ group + trials + sex + group:trials + group:sex + trials:sex + (1|rat_ID), df_R1)
eta_squared(leaving)

#calculate p value for interaction effects 
leaving_group_trials <- lmer(leaving.score~ group + trials + sex + group:sex + trials:sex + (1|rat_ID), df_R1)
leaving_group_sex <- lmer(leaving.score~ group + trials + sex + group:trials + trials:sex + (1|rat_ID), df_R1)
leaving_trials_sex <- lmer(leaving.score~ group + trials + sex + group:trials + group:sex + (1|rat_ID), df_R1)

anova(leaving,leaving_group_trials)
anova(leaving,leaving_group_sex)
anova(leaving,leaving_trials_sex)

#calculate p value for single effects 
leaving_group <- lmer(leaving.score~ trials + sex + trials:sex + (1|rat_ID), df_R1)
leaving_trials <- lmer(leaving.score~ group + sex + group:sex + (1|rat_ID), df_R1)
leaving_sex <- lmer(leaving.score~ group + trials + trials:sex + (1|rat_ID), df_R1)

anova(leaving,leaving_group)
anova(leaving,leaving_trials)
anova(leaving,leaving_sex)
```

##plot for R day1, leaving
```{r}
#to plot medium group effect on leaving performance
Fig_leaving<- ggplot(df_R1, aes(x=trials, y=leaving.score, group=rat_ID)) + geom_line(color="gray", alpha=0.2)+
stat_summary(fun=mean, aes(group = group, color= group), geom="line", linewidth=2)+
stat_summary(fun.data = mean_se, aes(group=group, color = group), geom = "errorbar", alpha=1, width=0.05)+
scale_color_manual(values = c("#66C2A5", "#FC8D62"))+
scale_y_continuous(breaks = seq(0, 7, by = 1))+
labs(x = "positions", y = "leaving score") + theme_classic()

Fig_fluo_leaving <- Fig_leaving

Fig_fluo_leaving<-plot(Fig_leaving, palette = pal,  rawplot.ylabel = "number of entry") 
ggsave('C:/Users/xiamen/Desktop/graduate_thesis_figs/fluo/Fig_fluo_leaving.svg', plot = Fig_fluo_leaving, device = 'svg',dpi = 300)

```

## approaching
```{r}
approaching <- lmer(approaching.score~ group + trials + sex + group:trials + group:sex + trials:sex + (1|rat_ID), df_R1)
eta_squared(approaching)

approaching_group_trials <- lmer(approaching.score~ group + trials + sex + group:sex + trials:sex + (1|rat_ID), df_R1)
approaching_group_sex <- lmer(approaching.score~ group + trials + sex + group:trials + trials:sex + (1|rat_ID), df_R1)
approaching_trials_sex <- lmer(approaching.score~ group + trials + sex + group:trials + group:sex + (1|rat_ID), df_R1)

anova(approaching,approaching_group_trials)
anova(approaching,approaching_group_sex)
anova(approaching,approaching_trials_sex)

approaching_group <- lmer(approaching.score~ trials + sex + trials:sex + (1|rat_ID), df_R1)
approaching_trials <- lmer(approaching.score~ group + sex + group:sex + (1|rat_ID), df_R1)
approaching_sex <- lmer(approaching.score~ group + trials + trials:sex + (1|rat_ID), df_R1)

anova(approaching,approaching_group)
anova(approaching,approaching_trials)
anova(approaching,approaching_sex)
```
## plot
```{r}
#to plot the group performance on approaching, although the effect from group is small
Fig_approaching<- ggplot(df_R1, aes(x=trials, y=approaching.score, group=rat_ID)) + geom_line(color="gray", alpha=0.2)+
stat_summary(fun=mean, aes(group = group, color= group), geom="line", linewidth=2)+
stat_summary(fun.data = mean_se, aes(group=group, color = group), geom = "errorbar", alpha=1, width=0.05)+
scale_color_manual(values = c("#8DA0CB", "#E78AC3"))+
scale_y_continuous(breaks = seq(0, 7, by = 1))+
labs(x = "positions", y = "approaching score") + theme_classic()
Fig_approaching

#because of medium sex effect, we plot the sex effect on approaching
Fig_approaching_sex<- ggplot(df_R1, aes(x=trials, y=approaching.score, group=rat_ID)) + geom_line(color="gray", alpha=0.2)+
stat_summary(fun=mean, aes(group = sex, color= sex), geom="line", linewidth=2)+
stat_summary(fun.data = mean_se, aes(group=sex, color = sex), geom = "errorbar", alpha=1, width=0.05)+
scale_color_manual(values = c("#8DA0CB", "#E78AC3"))+
scale_y_continuous(breaks = seq(0, 7, by = 1))+
labs(x = "positions", y = "approaching score") + theme_classic()
Fig_approaching_sex
```
## foraging
```{r}
foraging <- lmer(foraging.score~ group + trials + sex + group:trials + group:sex + trials:sex + (1|rat_ID), df_R1)
eta_squared(foraging)

foraging_group_trials <- lmer(foraging.score~ group + trials + sex + group:sex + trials:sex + (1|rat_ID), df_R1)
foraging_group_sex <- lmer(foraging.score~ group + trials + sex + group:trials + trials:sex + (1|rat_ID), df_R1)
foraging_trials_sex <- lmer(foraging.score~ group + trials + sex + group:trials + group:sex + (1|rat_ID), df_R1)

anova(foraging,foraging_group_trials)
anova(foraging,foraging_group_sex)
anova(foraging,foraging_trials_sex)

foraging_group <- lmer(foraging.score~ trials + sex + trials:sex + (1|rat_ID), df_R1)
foraging_trials <- lmer(foraging.score~ group + sex + group:sex + (1|rat_ID), df_R1)
foraging_sex <- lmer(foraging.score~ group + trials + trials:sex + (1|rat_ID), df_R1)

anova(foraging,foraging_group)
anova(foraging,foraging_trials)
anova(foraging,foraging_sex)
```

## plot
```{r}
Fig_foraging<- ggplot(df_R1, aes(x=trials, y=foraging.score, group=rat_ID)) + geom_line(color="gray", alpha=0.2)+
stat_summary(fun=mean, aes(group = group, color= group), geom="line", linewidth=2)+
stat_summary(fun.data = mean_se, aes(group=group, color = group), geom = "errorbar", alpha=1, width=0.05)+
scale_color_manual(values = c("#A6D854", "#FFD92F"))+
scale_y_continuous(breaks = seq(0, 7, by = 1))+
labs(x = "positions", y = "foraging score") + theme_classic() #+ facet_wrap(~group, ncol=2,labeller = labeller(group = c("WT" = "WT", "Fluoxetine" = "Fluoxetine")))
Fig_foraging

#because of medium sex effect, we plot the sex effect on foraging
Fig_foraging_sex<- ggplot(df_R1, aes(x=trials, y=foraging.score, group=rat_ID)) + geom_line(color="gray", alpha=0.2)+
stat_summary(fun=mean, aes(group = sex, color= sex), geom="line", linewidth=2)+
stat_summary(fun.data = mean_se, aes(group=sex, color = sex), geom = "errorbar", alpha=1, width=0.05)+
scale_color_manual(values = c("#8DA0CB", "#E78AC3"))+
scale_y_continuous(breaks = seq(0, 7, by = 1))+
labs(x = "positions", y = "foraging score") + theme_classic()
Fig_foraging_sex
```

## now compare Rday1 vs Rday2
```{r}
#leaving
leaving_R12 <- lmer(leaving.score~ group + trials + sex + robot.day + robot.day:group + robot.day:trials + robot.day:sex + group:trials + group:sex + trials:sex + (1|rat_ID), df)
eta_squared(leaving_R12)

#approaching
approaching_R12 <- lmer(approaching.score~ group + trials + sex + robot.day + robot.day:group + robot.day:trials + robot.day:sex + group:trials + group:sex + trials:sex + (1|rat_ID), df)
eta_squared(approaching_R12)

#foraging
foraging_R12 <- lmer(foraging.score~ group + trials + sex + robot.day + robot.day:group + robot.day:trials + robot.day:sex + group:trials + group:sex + trials:sex + (1|rat_ID), df)
eta_squared(foraging_R12)

##Figs
#leaving
df_success_rate <- read_csv('C:/Users/xiamen/Desktop/data_for_R/success_rate.csv')
df_success_rate_fluoxetine <- df_success_rate %>% filter(df_success_rate$class == "Fluoxetine")
df_success_rate_fluoxetine_leaving <- df_success_rate_fluoxetine %>% filter(df_success_rate_fluoxetine$behavior == "leaving")

Fig_leaving_success <- ggplot(df_success_rate_fluoxetine_leaving, aes(x=trials, y=success_rate, fill=group)) + 
  geom_col(position = position_dodge(width = 0.8), width = 0.6) + 
  scale_fill_manual(values = c("#114232", "#87A922","#FCDC2A","#F7F6BB"))+
  facet_wrap(~treatment, ncol=2)

Fig_leaving_success

#approaching
df_success_rate_fluoxetine_approaching <- df_success_rate_fluoxetine %>% filter(df_success_rate_fluoxetine$behavior == "approaching")

Fig_approaching_success <- ggplot(df_success_rate_fluoxetine_approaching, aes(x=trials, y=success_rate, fill=group)) + 
  geom_col(position = position_dodge(width = 0.8), width = 0.6) + 
  scale_fill_manual(values = c("#114232", "#87A922","#FCDC2A","#F7F6BB"))+
  facet_wrap(~treatment, ncol=2)

Fig_approaching_success

#for success rate of foraging
df_success_rate_fluoxetine_foraging <- df_success_rate_fluoxetine %>% filter(df_success_rate_fluoxetine$behavior == "foraging")

Fig_foraging_success <- ggplot(df_success_rate_fluoxetine_foraging, aes(x=trials, y=success_rate, fill=group)) + 
  geom_col(position = position_dodge(width = 0.8), width = 0.6) + 
  scale_fill_manual(values = c("#114232", "#87A922","#FCDC2A","#F7F6BB")) +
  facet_wrap(~ treatment, ncol = 2)
Fig_foraging_success
```