Stat_meng
================
Joanes Grandjean

## install necessary packages

Run this only once to have packages installed in your environment

``` r
install.packages('tidyverse', 'glue', 'glue','lme4','multcomp','parameters', 'effectsize','performance','ggpubr')
```

## load the packages

``` r
#General utility packages
library(tidyverse)
```

    ## ── Attaching packages ─────────────────────────────────────── tidyverse 1.3.1 ──

    ## ✓ ggplot2 3.3.5     ✓ purrr   0.3.4
    ## ✓ tibble  3.1.6     ✓ dplyr   1.0.7
    ## ✓ tidyr   1.1.4     ✓ stringr 1.4.0
    ## ✓ readr   2.1.0     ✓ forcats 0.5.1

    ## ── Conflicts ────────────────────────────────────────── tidyverse_conflicts() ──
    ## x dplyr::filter() masks stats::filter()
    ## x dplyr::lag()    masks stats::lag()

``` r
library(glue)
library(knitr)

#Stats packages
library(lme4)
```

    ## Loading required package: Matrix

    ## 
    ## Attaching package: 'Matrix'

    ## The following objects are masked from 'package:tidyr':
    ## 
    ##     expand, pack, unpack

``` r
library(multcomp)
```

    ## Loading required package: mvtnorm

    ## Loading required package: survival

    ## Loading required package: TH.data

    ## Loading required package: MASS

    ## 
    ## Attaching package: 'MASS'

    ## The following object is masked from 'package:dplyr':
    ## 
    ##     select

    ## 
    ## Attaching package: 'TH.data'

    ## The following object is masked from 'package:MASS':
    ## 
    ##     geyser

``` r
library(parameters)
library(effectsize)
library(performance)

#Plot packages
library(ggpubr)
```

``` r
#output session info
sessionInfo()
```

    ## R version 4.0.4 (2021-02-15)
    ## Platform: x86_64-pc-linux-gnu (64-bit)
    ## Running under: Pop!_OS 21.10
    ## 
    ## Matrix products: default
    ## BLAS:   /usr/lib/x86_64-linux-gnu/blas/libblas.so.3.9.0
    ## LAPACK: /usr/lib/x86_64-linux-gnu/lapack/liblapack.so.3.9.0
    ## 
    ## locale:
    ##  [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C              
    ##  [3] LC_TIME=en_US.UTF-8        LC_COLLATE=en_US.UTF-8    
    ##  [5] LC_MONETARY=en_US.UTF-8    LC_MESSAGES=en_US.UTF-8   
    ##  [7] LC_PAPER=en_US.UTF-8       LC_NAME=C                 
    ##  [9] LC_ADDRESS=C               LC_TELEPHONE=C            
    ## [11] LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C       
    ## 
    ## attached base packages:
    ## [1] stats     graphics  grDevices utils     datasets  methods   base     
    ## 
    ## other attached packages:
    ##  [1] ggpubr_0.4.0      performance_0.8.0 effectsize_0.5    parameters_0.15.0
    ##  [5] multcomp_1.4-17   TH.data_1.1-0     MASS_7.3-53.1     survival_3.2-10  
    ##  [9] mvtnorm_1.1-3     lme4_1.1-27.1     Matrix_1.3-2      knitr_1.36       
    ## [13] glue_1.6.1        forcats_0.5.1     stringr_1.4.0     dplyr_1.0.7      
    ## [17] purrr_0.3.4       readr_2.1.0       tidyr_1.1.4       tibble_3.1.6     
    ## [21] ggplot2_3.3.5     tidyverse_1.3.1  
    ## 
    ## loaded via a namespace (and not attached):
    ##  [1] httr_1.4.2        jsonlite_1.7.2    splines_4.0.4     carData_3.0-4    
    ##  [5] modelr_0.1.8      datawizard_0.2.1  assertthat_0.2.1  cellranger_1.1.0 
    ##  [9] bayestestR_0.11.5 yaml_2.2.1        pillar_1.6.4      backports_1.4.0  
    ## [13] lattice_0.20-41   digest_0.6.29     ggsignif_0.6.3    rvest_1.0.2      
    ## [17] minqa_1.2.4       colorspace_2.0-2  sandwich_3.0-1    htmltools_0.5.2  
    ## [21] pkgconfig_2.0.3   broom_0.7.10      haven_2.4.3       scales_1.1.1     
    ## [25] tzdb_0.2.0        car_3.0-12        generics_0.1.1    ellipsis_0.3.2   
    ## [29] withr_2.4.3       cli_3.1.1         magrittr_2.0.1    crayon_1.4.2     
    ## [33] readxl_1.3.1      evaluate_0.14     fs_1.5.0          fansi_1.0.2      
    ## [37] nlme_3.1-152      rstatix_0.7.0     xml2_1.3.2        tools_4.0.4      
    ## [41] hms_1.1.1         lifecycle_1.0.1   munsell_0.5.0     reprex_2.0.1     
    ## [45] compiler_4.0.4    rlang_0.4.12      grid_4.0.4        nloptr_1.2.2.3   
    ## [49] rstudioapi_0.13   rmarkdown_2.11    boot_1.3-27       gtable_0.3.0     
    ## [53] codetools_0.2-18  abind_1.4-5       DBI_1.1.1         R6_2.5.1         
    ## [57] zoo_1.8-9         lubridate_1.8.0   fastmap_1.1.0     utf8_1.2.2       
    ## [61] insight_0.14.5    stringi_1.7.6     Rcpp_1.0.8        vctrs_0.3.8      
    ## [65] dbplyr_2.1.1      tidyselect_1.1.1  xfun_0.28

## Load and inspect the table

``` r
df <- read_csv('data.csv') %>% 
          mutate(day.trial = as.factor(glue("{Baseline.day}.{Trails}"))) %>% 
          mutate(group = as.factor(group)) %>% 
          mutate(Gender = as.factor(Gender)) %>% 
          mutate(Rat_ID = as.factor(Rat_ID)) %>% 
        # Create score based on time. Following log attribution of score as a function of time. 
          mutate(time.out.nest.score =  case_when(is.na(time.out.nest.sec) ~ 6,
                                                  time.out.nest.sec >= 200 ~ 5,
                                                  time.out.nest.sec >= 100 ~ 4,
                                                  time.out.nest.sec >= 50 ~ 3,
                                                  time.out.nest.sec >= 25  ~ 2,
                                                  time.out.nest.sec >= 12.5 ~ 1,
                                                  time.out.nest.sec >= 6.25 ~ 0)) %>% 
            mutate(time.to.food.score =  case_when(is.na(time.to.food.sec) ~ 6,
                                                  time.to.food.sec >= 200 ~ 5,
                                                  time.to.food.sec >= 100 ~ 4,
                                                  time.to.food.sec >= 50 ~ 3,
                                                  time.to.food.sec >= 25  ~ 2,
                                                  time.to.food.sec >= 12.5 ~ 1,
                                                  time.to.food.sec >= 6.25 ~ 0)) %>% 
            mutate(time.to.nest.score =  case_when(is.na(time.to.nest.sec) ~ 6,
                                                  time.to.nest.sec >= 200 ~ 5,
                                                  time.to.nest.sec >= 100 ~ 4,
                                                  time.to.nest.sec >= 50 ~ 3,
                                                  time.to.nest.sec >= 25  ~ 2,
                                                  time.to.nest.sec >= 12.5 ~ 1,
                                                  time.to.nest.sec >= 6.25 ~ 0))  
```

    ## Rows: 672 Columns: 11

    ## ── Column specification ────────────────────────────────────────────────────────
    ## Delimiter: ","
    ## chr (2): Gender, group
    ## dbl (9): Rat_ID, Baseline.day, Robot.day, Trails, food.distance.cm, got.food...

    ## 
    ## ℹ Use `spec()` to retrieve the full column specification for this data.
    ## ℹ Specify the column types or set `show_col_types = FALSE` to quiet this message.

``` r
#head(df) %>% kable("pipe")
```

## Summary of the table

``` r
summary(df) %>% kable("pipe")
```

|     | Rat_ID      | Baseline.day | Robot.day      | Trails        | Gender | group         | food.distance.cm | got.food       | time.out.nest.sec | time.to.food.sec | time.to.nest.sec | day.trial   | time.out.nest.score | time.to.food.score | time.to.nest.score |
|:----|:------------|:-------------|:---------------|:--------------|:-------|:--------------|:-----------------|:---------------|:------------------|:-----------------|:-----------------|:------------|:--------------------|:-------------------|:-------------------|
|     | 405305 : 28 | Min. :0.00   | Min. :0.0000   | Min. :1.000   | F:336  | Corhort 1:336 | Min. : 25.40     | Min. :0.0000   | Min. : 1.00       | Min. : 2.00      | Min. : 3.00      | 0.1 : 48    | Min. :0.000         | Min. :0.000        | Min. :0.000        |
|     | 405306 : 28 | 1st Qu.:0.00 | 1st Qu.:0.0000 | 1st Qu.:1.000 | M:336  | Corhort 2:336 | 1st Qu.: 25.40   | 1st Qu.:1.0000 | 1st Qu.: 4.00     | 1st Qu.: 7.75    | 1st Qu.: 13.00   | 0.2 : 48    | 1st Qu.:0.000       | 1st Qu.:0.000      | 1st Qu.:1.000      |
|     | 405307 : 28 | Median :2.00 | Median :0.0000 | Median :2.000 |        |               | Median : 50.80   | Median :1.0000 | Median : 7.00     | Median : 15.00   | Median : 27.00   | 0.3 : 48    | Median :1.000       | Median :1.000      | Median :2.000      |
|     | 405308 : 28 | Mean :2.25   | Mean :0.5357   | Mean :2.357   |        |               | Mean : 59.87     | Mean :0.9091   | Mean : 15.71      | Mean : 31.80     | Mean : 53.48     | 0.4 : 48    | Mean :1.071         | Mean :1.582        | Mean :2.309        |
|     | 405309 : 28 | 3rd Qu.:4.00 | 3rd Qu.:1.0000 | 3rd Qu.:3.000 |        |               | 3rd Qu.: 76.20   | 3rd Qu.:1.0000 | 3rd Qu.: 15.00    | 3rd Qu.: 37.25   | 3rd Qu.: 67.00   | 0.5 : 48    | 3rd Qu.:2.000       | 3rd Qu.:2.000      | 3rd Qu.:4.000      |
|     | 405310 : 28 | Max. :6.00   | Max. :2.0000   | Max. :5.000   |        |               | Max. :127.00     | Max. :1.0000   | Max. :212.00      | Max. :308.00     | Max. :310.00     | 1.1 : 24    | Max. :6.000         | Max. :6.000        | Max. :6.000        |
|     | (Other):504 |              |                |               |        |               |                  | NA’s :1        | NA’s :1           | NA’s :12         | NA’s :61         | (Other):408 | NA’s :318           | NA’s :129          | NA’s :25           |

## Let’s plot time to food, either in sec or in score on Robotgator day 1

``` r
df.baseline <- df %>% filter(df$Robot.day == 1)


p <- ggplot(df.baseline, aes(x=day.trial, y=time.to.food.sec, group=group, colour=group, shape=Gender))

p + stat_summary(fun = "mean", colour = "darkgrey", size = 0.5, geom = "line", fun.min = 'sd', fun.max = 'sd')+
    stat_summary(fun.data = mean_se, geom = "ribbon", alpha=0.3, aes(colour = group, fill = group))+
      geom_point(alpha = 1)+theme_minimal()
```

![](README_files/figure-gfm/unnamed-chunk-5-1.png)<!-- -->

``` r
p <- ggplot(df.baseline, aes(x=day.trial, y=time.to.food.score, group=group, colour=group, shape=Gender))

p + stat_summary(fun = "mean", colour = "darkgrey", size = 0.5, geom = "line", fun.min = 'sd', fun.max = 'sd')+
    stat_summary(fun.data = mean_se, geom = "ribbon", alpha=0.3, aes(colour = group, fill = group))+
      geom_point(alpha = 1)+theme_minimal()
```

![](README_files/figure-gfm/unnamed-chunk-6-1.png)<!-- -->

The score seem to recapitulate the time in seconds. Maybe worth using it
for stats?

## Comparison of linear mixed models with time in sec vs time in score

``` r
mod.sec <- lmer(time.to.food.sec~ day.trial:Gender  + group + day.trial + Gender + (1|Rat_ID), df.baseline)
mod.score <- lmer(time.to.food.score~ day.trial:Gender  + group + day.trial + Gender + (1|Rat_ID), df.baseline)
# checking model assumptions
compare_performance(mod.sec, mod.score)
```

    ## # Comparison of Model Performance Indices
    ## 
    ## Name      |   Model |      AIC | AIC weights |      BIC | BIC weights | R2 (cond.) | R2 (marg.) |   ICC |   RMSE |  Sigma
    ## -------------------------------------------------------------------------------------------------------------------------
    ## mod.sec   | lmerMod | 1061.523 |     < 0.001 | 1097.094 |     < 0.001 |      0.342 |      0.221 | 0.155 | 27.383 | 30.274
    ## mod.score | lmerMod |  362.701 |        1.00 |  396.697 |        1.00 |      0.460 |      0.321 | 0.205 |  1.110 |  1.250

The model with scores has a lower AIC and BIC, but comparable R2. This
suggest the model with scores is preferable.

What about the assumptions

``` r
print('checking model with seconds')
check_normality(mod.sec)
check_heteroscedasticity(mod.sec)
print('checking model with scores')
check_normality(mod.score)
check_heteroscedasticity(mod.score)
```

    ## [1] "checking model with seconds"
    ## Warning: Non-normality of residuals detected (p < .001).
    ## Warning: Heteroscedasticity (non-constant error variance) detected (p < .001).
    ## [1] "checking model with scores"
    ## OK: residuals appear as normally distributed (p = 0.265).
    ## OK: Error variance appears to be homoscedastic (p = 0.112).

Model with scores seem to better follow normal distribution.

``` r
check_model(mod.score,check=c("vif", "qq", "normality","linearity","homogeneity",'reqq'))
```

![](README_files/figure-gfm/unnamed-chunk-9-1.png)<!-- -->

Diagnostic plot is a bit odd, but this is to be expected given the
response variable is binned into score. Also, colinearity between trial
and trial:gender interaction effect is to be expected, so I suppose this
is fine.

``` r
mod.score1<- lmer(time.to.food.score~ Gender:group+ day.trial:Gender  + group + day.trial + Gender + (1|Rat_ID), df.baseline)

mod.score2<- lmer(time.to.food.score~ day.trial:group +  day.trial:Gender  + group + day.trial + Gender + (1|Rat_ID), df.baseline)

mod.score3<- lmer(time.to.food.score~ day.trial:group + Gender:group+ day.trial:Gender  + group + day.trial + Gender + (1|Rat_ID), df.baseline)


compare_performance(mod.score, mod.score1, mod.score2, mod.score3)
```

    ## # Comparison of Model Performance Indices
    ## 
    ## Name       |   Model |     AIC | AIC weights |     BIC | BIC weights | R2 (cond.) | R2 (marg.) |   ICC |  RMSE | Sigma
    ## ----------------------------------------------------------------------------------------------------------------------
    ## mod.score  | lmerMod | 362.701 |       0.019 | 396.697 |       0.270 |      0.460 |      0.321 | 0.205 | 1.110 | 1.250
    ## mod.score1 | lmerMod | 358.151 |       0.186 | 394.763 |       0.710 |      0.456 |      0.378 | 0.125 | 1.130 | 1.256
    ## mod.score2 | lmerMod | 360.273 |       0.065 | 404.730 |       0.005 |      0.484 |      0.346 | 0.212 | 1.066 | 1.234
    ## mod.score3 | lmerMod | 355.420 |       0.730 | 402.493 |       0.015 |      0.481 |      0.405 | 0.129 | 1.085 | 1.238

Trying to add additional interaction effects to see if this improves our
model. We could consider adding these interactions in our model.

## Test for standardized effects

reminder to read <https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6646942/>
?

``` r
print('standardized coeficient for model with sec')
eta_squared(mod.sec)
print('')
print('standardized coeficient for model with score')
eta_squared(mod.score)
```

    ## [1] "standardized coeficient for model with sec"
    ## # Effect Size for ANOVA (Type III)
    ## 
    ## Parameter        | Eta2 (partial) |       95% CI
    ## ------------------------------------------------
    ## group            |           0.15 | [0.00, 1.00]
    ## day.trial        |           0.25 | [0.10, 1.00]
    ## Gender           |           0.03 | [0.00, 1.00]
    ## day.trial:Gender |           0.05 | [0.00, 1.00]
    ## 
    ## - One-sided CIs: upper bound fixed at (1).[1] ""
    ## [1] "standardized coeficient for model with score"
    ## # Effect Size for ANOVA (Type III)
    ## 
    ## Parameter        | Eta2 (partial) |       95% CI
    ## ------------------------------------------------
    ## group            |           0.19 | [0.01, 1.00]
    ## day.trial        |           0.37 | [0.21, 1.00]
    ## Gender           |           0.03 | [0.00, 1.00]
    ## day.trial:Gender |           0.07 | [0.00, 1.00]
    ## 
    ## - One-sided CIs: upper bound fixed at (1).

Here is a key to interpret the table, according to
<https://imaging.mrc-cbu.cam.ac.uk/statswiki/FAQ/effectSize>

Eta2 for Multiple Regression  
small = 0.02  
medium = 0.13  
large = 0.26

Conclusion, ‘group’ has a medium effect, day.trial has strong effect.
Gender has a small effect, and interaction between day and gender is
also small.

``` r
eta_squared(mod.score3)
```

    ## # Effect Size for ANOVA (Type III)
    ## 
    ## Parameter        | Eta2 (partial) |       95% CI
    ## ------------------------------------------------
    ## group            |           0.22 | [0.01, 1.00]
    ## day.trial        |           0.35 | [0.18, 1.00]
    ## Gender           |           0.02 | [0.00, 1.00]
    ## day.trial:group  |           0.08 | [0.00, 1.00]
    ## group:Gender     |           0.26 | [0.03, 1.00]
    ## day.trial:Gender |           0.08 | [0.00, 1.00]
    ## 
    ## - One-sided CIs: upper bound fixed at (1).

adding additional interactions reveal a large group:gender interaction
effect, and small trail:group interaction.

Perso, I would stop here. I would show effect size with 90% CI. I
understand that some p-values are desirable, so let’s try it out.

## Test for significance.

``` r
mod.interaction <-  update(mod.score, . ~ . - day.trial:Gender)

anova(mod.score, mod.interaction)
```

    ## refitting model(s) with ML (instead of REML)

    ## Data: df.baseline
    ## Models:
    ## mod.interaction: time.to.food.score ~ group + day.trial + Gender + (1 | Rat_ID)
    ## mod.score: time.to.food.score ~ day.trial:Gender + group + day.trial + Gender + (1 | Rat_ID)
    ##                 npar    AIC    BIC  logLik deviance  Chisq Df Pr(>Chisq)
    ## mod.interaction    9 360.91 384.44 -171.45   342.91                     
    ## mod.score         13 363.37 397.36 -168.68   337.37 5.5369  4     0.2365

We cannot conclude that there is a trial X session interaction effect

2.  let’s see if there is a trial effect

``` r
mod.trial <- update(mod.interaction, . ~ . - day.trial)

anova(mod.interaction, mod.trial)
```

    ## refitting model(s) with ML (instead of REML)

    ## Data: df.baseline
    ## Models:
    ## mod.trial: time.to.food.score ~ group + Gender + (1 | Rat_ID)
    ## mod.interaction: time.to.food.score ~ group + day.trial + Gender + (1 | Rat_ID)
    ##                 npar    AIC    BIC  logLik deviance  Chisq Df Pr(>Chisq)    
    ## mod.trial          5 387.58 400.66 -188.79   377.58                         
    ## mod.interaction    9 360.91 384.44 -171.45   342.91 34.678  4   5.41e-07 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

We do find a strong session effect (F(5,9)=34.678, p \< 0.001).
Concurrently with the plot above, we can conclude that the rats get
faster over time to get the food.

4.  let’s see if there is a sex effect (ill-named gender!!!)

``` r
mod.sex <- update(mod.interaction, . ~ . - Gender)

anova(mod.interaction, mod.sex)
```

    ## refitting model(s) with ML (instead of REML)

    ## Data: df.baseline
    ## Models:
    ## mod.sex: time.to.food.score ~ group + day.trial + (1 | Rat_ID)
    ## mod.interaction: time.to.food.score ~ group + day.trial + Gender + (1 | Rat_ID)
    ##                 npar    AIC    BIC  logLik deviance  Chisq Df Pr(>Chisq)
    ## mod.sex            8 360.03 380.95 -172.01   344.03                     
    ## mod.interaction    9 360.91 384.44 -171.45   342.91 1.1207  1     0.2898

There is a no sex effect there.

5.  let’s see if there is a group effect

``` r
mod.group <- update(mod.interaction, . ~ . - group)

anova(mod.interaction, mod.group)
```

    ## refitting model(s) with ML (instead of REML)

    ## Data: df.baseline
    ## Models:
    ## mod.group: time.to.food.score ~ day.trial + Gender + (1 | Rat_ID)
    ## mod.interaction: time.to.food.score ~ group + day.trial + Gender + (1 | Rat_ID)
    ##                 npar    AIC    BIC  logLik deviance  Chisq Df Pr(>Chisq)  
    ## mod.group          8 364.70 385.62 -174.35   348.70                       
    ## mod.interaction    9 360.91 384.44 -171.45   342.91 5.7983  1    0.01604 *
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

In line with our effect size estimation above, there is a medium group
effect.

## Alternative test, Chi-squared?

``` r
# Define color palette
my_cols <- c("#0D0887FF", "#6A00A8FF", "#B12A90FF",
"#E16462FF", "#FCA636FF", "#F0F921FF")

Chi.prep <- df.baseline %>% group_by(group, Trails) %>% tally(got.food==TRUE)  %>%
  pivot_wider(names_from = group, values_from = n)


#plot contingency table
Chi.prep %>%  dplyr::select(`Corhort 1`,`Corhort 2`) %>% ggballoonplot(Chi.prep, fill = "value")+
   scale_fill_gradientn(colors = my_cols)
```

![](README_files/figure-gfm/unnamed-chunk-17-1.png)<!-- -->

``` r
chisq.test(Chi.prep) 
```

    ## 
    ##  Pearson's Chi-squared test
    ## 
    ## data:  Chi.prep
    ## X-squared = 7.3257, df = 8, p-value = 0.5019

Overall, not a good idea to run Chi-square test. Also, we loose in
granularity.

## Group size estimation.

In this section, we resample the existing data and add varying effects
to test robustness of our estimations with different group size.

``` r
nperm<-500
group_size_list<-c(6, 10, 14)  # we test these group sizes
effect_list<-c(1, 2, 3)  #not actual effect sizes, but multiplicators of existing data. Fine tuned so it corresponds to actual effect sizes. 

df.statsum<-tibble(effect=double(),
                 group_size=double(),
                fixed_effect=double(),
                 issig=integer())

for(group_size in group_size_list){
for(effect in effect_list){
  for(n in 1:nperm){

df.new <- df.baseline[0,]


for(sex in c('F', 'M')){
    for(group in c('ctl', 'exp')){
      for(i in 1:group_size){
        newratID<-glue('{i}_{group}_{sex}')
      sampleID<-sample(unique(df.baseline$Rat_ID[df.baseline$Gender==sex]),1)
      df.tmp<-df.baseline %>%  filter(df.baseline$Rat_ID==sampleID)
        
      df.tmp$Rat_ID<-as.character(newratID)
      df.tmp$group<-group
      if(group=='exp'){df.tmp$time.to.food.sec<-df.tmp$time.to.food.sec*(effect+rnorm(1))}
      
      df.new<-df.new %>% add_row(df.tmp)
      
      }
    }
}

df.new <- df.new  %>%  mutate(time.to.food.score =  case_when(is.na(time.to.food.sec) ~ 6,
                                                  time.to.food.sec >= 200 ~ 5,
                                                  time.to.food.sec >= 100 ~ 4,
                                                  time.to.food.sec >= 50 ~ 3,
                                                  time.to.food.sec >= 25  ~ 2,
                                                  time.to.food.sec >= 12.5 ~ 1,
                                                  time.to.food.sec >= 6.25 ~ 0)) 

mod<-lmer(time.to.food.score~group + day.trial + Gender + + (1|Rat_ID),df.new, verbose = 0 )
mod.alt<-lmer(time.to.food.score~day.trial + Gender + + (1|Rat_ID),df.new, verbose = 0  )


fixed_effect<-eta_squared(mod)$Eta2_partial[1]
is.sig<-as.integer(anova(mod,mod.alt, refit=FALSE)$`Pr(>Chisq)`[2] < 0.05)

df.statsum<- df.statsum %>% add_row(effect=effect, group_size=group_size, fixed_effect=fixed_effect,issig=is.sig)
}
}
}

df.statsum <- df.statsum %>% mutate(effect=as_factor(effect), group_size=as_factor(group_size)) %>% mutate(effect=fct_recode(effect, small='1', medium='2', large='3'))
df.statsum %>%  group_by(effect,group_size) %>% summarise(fixed_effect=mean(fixed_effect), is.sig=mean(issig))

 p<-ggplot(df.statsum, aes(x=group_size, y=fixed_effect, fill=effect))
 p+geom_hline(yintercept = 0.02,linetype='dotted')+geom_hline(yintercept = 0.13,linetype='dotted')+geom_hline(yintercept = 0.26,linetype='dotted')+geom_boxplot()+labs(y='effective effect size', x='group size')+ theme(legend.position="bottom")+theme_bw()
```

![](README_files/figure-gfm/unnamed-chunk-18-1.png)<!-- -->

    ## # A tibble: 9 × 4
    ## # Groups:   effect [3]
    ##   effect group_size fixed_effect is.sig
    ##   <fct>  <fct>             <dbl>  <dbl>
    ## 1 small  6                0.0868  0.14 
    ## 2 small  10               0.0532  0.132
    ## 3 small  14               0.0480  0.182
    ## 4 medium 6                0.145   0.37 
    ## 5 medium 10               0.137   0.556
    ## 6 medium 14               0.127   0.662
    ## 7 large  6                0.286   0.788
    ## 8 large  10               0.264   0.928
    ## 9 large  14               0.263   0.986
